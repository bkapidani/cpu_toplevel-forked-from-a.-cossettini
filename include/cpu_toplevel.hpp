#ifndef CPU_TOPLEVEL_HPP
#define CPU_TOPLEVEL_HPP

#include <systemc.h>
#include "alu.hpp"
#include "adder.hpp"
#include "Memory_16b_RISC.hpp"  
#include "control_unit.hpp"
#include "register_file.hpp"
#include "program_counter.hpp"
#include "multiplexer_2x3.hpp"
#include "multiplexer_2x16.hpp"
#include "multiplexer_3x16.hpp"
#include "left_shift_6.hpp"
#include "sign_extend_7.hpp"
#include "Control_Word.hpp"
#include "T_Register.hpp"

using namespace std;

SC_MODULE(Cpu_toplevel)
{
   // inputs and outputs
   sc_in<bool> clock;
   sc_in<sc_bv<1> > load;  //Loads the current instruction word
   sc_in<sc_bv<1> > zero_for_we_imem;
   sc_in<sc_bv<16> > zero_for_data_in_imem;
   sc_in<sc_bv<16> > const1;

   // internal signals
   sc_signal<sc_bv<16> > PC_in;
   sc_signal<sc_bv<16> > PC_out;
   sc_signal<sc_bv<16> > add1_out;
   sc_signal<sc_bv<16> > add_out;
   sc_signal<sc_bv<16> > instr_out;
   sc_signal<sc_bv<16> > sig_instr_in;

   sc_signal<sc_bv<3> > sig_opcode;
   sc_signal<sc_bv<3> > sig_tgt_addr;
   sc_signal<sc_bv<3> > sig_src1_addr;
   sc_signal<sc_bv<3> > sig_mux2x3_in1;
   sc_signal<sc_bv<3> > sig_mux2x3_in2;
   sc_signal<sc_bv<16> > sig_shifter1_ingresso;
   sc_signal<sc_bv<7> > sig_sign_ext1_ingresso;
   sc_signal<sc_bv<7> > sig_sign_ext2_ingresso;

   sc_signal<sc_bv<3> > pipeline_sig_tgt_addr;
   T_Register<sc_bv<3> > reg_tgt_addr;
   
   sc_signal<sc_bv<3> > double_pipeline_sig_tgt_addr;
   T_Register<sc_bv<3> > double_reg_tgt_addr;

   sc_signal<sc_bv<3> > pipeline_sig_src1_addr;
   T_Register<sc_bv<3> > reg_src1_addr;

   sc_signal<sc_bv<3> > pipeline_sig_mux2x3_in1;
   T_Register<sc_bv<3> > reg_mux2x3_in1;

   sc_signal<sc_bv<3> > pipeline_sig_mux2x3_in2;
   T_Register<sc_bv<3> > reg_mux2x3_in2;

   sc_signal<sc_bv<16> > pipeline_sig_shifter1_ingresso;
   T_Register<sc_bv<16> > reg_shifter1;

   //~ sc_signal<sc_bv<7> > pipeline_sig_sign_ext1_ingresso;
   //~ T_Register<sc_bv<7> > reg_sign_ext1;

   sc_signal<sc_bv<7> > pipeline_sig_sign_ext2_ingresso;
   T_Register<sc_bv<7> > reg_sign_ext2;
   
   sc_signal<sc_bv<16> > pipeline_reg_tgt;
   T_Register<sc_bv<16> > reg_rf_tgt;

   sc_signal<sc_bv<16> > pipeline_ext_add;
   T_Register<sc_bv<16> > reg_sign_ext_PC;
   
   //~ sc_signal<sc_bv<16> > pipeline_add1_out;
   //~ T_Register<sc_bv<16> > reg_add1_out;

   //sc_signal<sc_bv<16> > pipeline_add1_out;
   //T_Register<sc_bv<16> > reg_adder2;

   sc_signal<sc_bv<16> > ext_add;
   sc_signal<sc_bv<16> > reg_tgt;
   sc_signal<sc_bv<3> > reg_in2;
   sc_signal<sc_bv<16> > reg_out1;
   sc_signal<sc_bv<16> > reg_out2;
   sc_signal<sc_bv<16> > shift_out;
   sc_signal<sc_bv<16> > ext_alu;
   sc_signal<sc_bv<16> > ALU_in1;
   sc_signal<sc_bv<16> > ALU_in2;
   sc_signal<sc_bv<16> > ALU_out;
   sc_signal<sc_bv<1> > ALU_outeq;
   sc_signal<sc_bv<16> > data_out;



   // internal control signals
   sc_signal<sc_bv<2> > mux_pc;
   sc_signal<sc_bv<1> > mux_rf;
   sc_signal<sc_bv<2> > mux_tgt;
   sc_signal<sc_bv<1> > we_rf;
   sc_signal<sc_bv<1> > we_dmem;
   sc_signal<sc_bv<1> > re_dmem;
   sc_signal<sc_bv<1> > mux_alu1;
   sc_signal<sc_bv<1> > mux_alu2;
   sc_signal<sc_bv<2> > func_alu;

   //~ sc_signal<sc_bv<2> > pipeline_mux_pc;
   //~ T_Register<sc_bv<2> > reg_mux_pc;
   
   sc_signal<sc_bv<1> > pipeline_mux_rf;
   T_Register<sc_bv<1> > reg_mux_rf;
   //T_Register<sc_bv<1> > reg_mux_rf_dummy;

   sc_signal<sc_bv<2> > pipeline_mux_tgt;
   T_Register<sc_bv<2> > reg_mux_tgt;

   sc_signal<sc_bv<1> > pipeline_we_rf;
   T_Register<sc_bv<1> > reg_we_rf;

   sc_signal<sc_bv<1> > double_pipeline_we_rf;
   T_Register<sc_bv<1> > double_reg_we_rf;

   sc_signal<sc_bv<1> > pipeline_we_dmem;
   T_Register<sc_bv<1> > reg_we_dmem;

   sc_signal<sc_bv<1> > pipeline_re_dmem;
   T_Register<sc_bv<1> > reg_re_dmem;

   sc_signal<sc_bv<1> > pipeline_mux_alu1;
   T_Register<sc_bv<1> > reg_mux_alu1;

   sc_signal<sc_bv<1> > pipeline_mux_alu2;
   T_Register<sc_bv<1> > reg_mux_alu2;

   sc_signal<sc_bv<2> > pipeline_func_alu;
   T_Register<sc_bv<2> > reg_func_alu;

   sc_signal<bool> control_ready;  // connects ControlUnit to PC and InstrMemory
   sc_signal<bool> register_file_ready;  // connects RF to DataMemory

   // components
   Control_Unit cu1;
   Program_Counter pc1;
   Register_File reg1;
   Control_Word reg_instr_out;
   Alu alu1;
   Adder adder1; 			// increment +1
   Adder adder2; 			// Program Counter increment BEQ
   Memory_16b_RISC memory_data;  	// to be implemented
   Memory_16b_RISC memory_instr;  	// to be implemented
   Multiplexer_2x3 mux2x3; 		// src2 of Register File multiplexer
   Multiplexer_2x16 mux2x16_1; 		// src1 of ALU multiplexer
   Multiplexer_2x16 mux2x16_2; 		// src2 of ALU multiplexer
   Multiplexer_3x16 mux3x16_1; 		// Program Counter multiplexer
   Multiplexer_3x16 mux3x16_2; 		// TGT Register File multiplexer
   Left_Shift_6 shifter1;
   Sign_Extend_7 sign_ext1;  		// adder PC sign_extend
   Sign_Extend_7 sign_ext2;  		// ALU mux src2 sign_extend

   Cpu_toplevel(sc_module_name name);
};

#endif
