#ifndef CONTROL_WORD_HPP
#define CONTROL_WORD_HPP

#include <systemc.h>

using namespace std;

SC_MODULE(Control_Word)          // module declaration
{
   sc_in<bool> clock;
   sc_in<sc_bv<16> > in;
   sc_out<sc_bv<3> > out_opcode;
   sc_out<sc_bv<3> > out_tgt_addr;
   sc_out<sc_bv<3> > out_src1_addr;
   sc_out<sc_bv<3> > out_mux2x3_in1;
   sc_out<sc_bv<3> > out_mux2x3_in2;
   sc_out<sc_bv<16> > out_shifter1_ingresso;
   sc_out<sc_bv<7> > out_sign_ext1_ingresso;
   sc_out<sc_bv<7> > out_sign_ext2_ingresso;

   // methods
   void route();
    
   //Constructor
   SC_CTOR(Control_Word) {

	SC_METHOD(route); 
 	sensitive << clock.pos();

   }

};

#endif

