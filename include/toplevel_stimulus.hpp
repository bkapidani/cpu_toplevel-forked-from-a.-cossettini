#include <systemc.h>
#include <string.h>
#include "alu.hpp"
#include "adder.hpp"
#include "Memory_16b_RISC.hpp"  
#include "control_unit.hpp"
#include "register_file.hpp"
#include "program_counter.hpp"
#include "multiplexer_2x3.hpp"
#include "multiplexer_2x16.hpp"
#include "multiplexer_3x16.hpp"
#include "left_shift_6.hpp"
#include "sign_extend_7.hpp"
#include "Control_Word.hpp"


SC_MODULE(toplevel_stimulus){

	//Declaration of inputs
	sc_in<bool> clock;
	unsigned num_instr;
        sc_in<sc_bv<3> > operation;

	//Declaration of outputs
//	sc_out<sc_bv<16> > ret_value;
	sc_out<sc_bv<1> > load;
	sc_out<sc_bv<1> > zero;
	sc_out<sc_bv<16> > zero_bv;
        sc_out<sc_bv<16> > const1;

	void stimgen();

	SC_CTOR(toplevel_stimulus){

		SC_THREAD(stimgen);
		sensitive << clock.pos();	

	}

};

