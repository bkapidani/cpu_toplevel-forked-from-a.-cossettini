#include "toplevel_stimulus.hpp"

using namespace std;


void toplevel_stimulus::stimgen()
{
	unsigned curr_instr=0;
	
	load = 0;
	const1 = "0000000000000001";
	curr_instr++;
	zero="0";
	zero_bv="0000000000000000";

	wait(SC_ZERO_TIME);

	cout << "toplevel_stimulus @ " << sc_time_stamp() << endl;
	cout << "	Clock = " << clock << endl;
	cout << "	load = " << load << endl;
	cout << "	opcode = " << operation << endl << endl << endl;		

//		cout << "	Output value equals " << ret_value.read().to_uint() << endl;

	wait();
			

	while(true)
	{		
		load = 1;
		const1 = "0000000000000001";
		curr_instr++;
		zero="0";
		zero_bv="0000000000000000";
	
		wait(SC_ZERO_TIME);

		cout << "toplevel_stimulus @ " << sc_time_stamp() << endl;
		cout << "	Clock = " << clock << endl;
		cout << "	load = " << load << endl;
		cout << "	opcode = " << operation << endl << endl << endl;		

//		cout << "	Output value equals " << ret_value.read().to_uint() << endl;

		wait();
                
 		if (operation.read() == "111")
		{
			cout << "	Number of instructions performed: " << curr_instr << endl;
			
			wait(); //empty cycle, to let instruction before JALR finish write-back
			sc_stop();
			return;
		}


	}

}


