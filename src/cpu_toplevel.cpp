// CPU_TOPLEVEL

#include <systemc.h>
#include "cpu_toplevel.hpp"

using namespace std;

SC_HAS_PROCESS(Cpu_toplevel);

Cpu_toplevel::Cpu_toplevel(sc_module_name name) :
   sc_module(name), cu1("cu1"), pc1("pc1"), reg1("reg1"), /*reg_add1_out("reg_add1_out"),*/reg_instr_out("reg_instr_out"), reg_rf_tgt("reg_rf_tgt"), double_reg_tgt_addr("double_reg_tgt_addr"), alu1("alu1"), adder1("adder1"), adder2("adder2"), memory_data("memory_data"), memory_instr("memory_instr"), mux2x3("mux2x3"), mux2x16_1("mux2x16_1"), mux2x16_2("mux2x16_2"), mux3x16_1("mux3x16_1"), mux3x16_2("mux3x16_2"), shifter1("shifter1"), sign_ext1("sign_ext1"), sign_ext2("sign_ext2"), reg_tgt_addr("reg_tgt_addr"), reg_src1_addr("reg_src1_addr"), reg_mux2x3_in1("reg_mux2x3_in1"), reg_mux2x3_in2("reg_mux2x3_in2"), reg_shifter1("reg_shifter1"), /*reg_sign_ext1("reg_sign_ext1"),*/ reg_sign_ext2("reg_sign_ext2"),/*reg_mux_pc("reg_mux_pc"),*/ reg_mux_rf("reg_mux_rf"), reg_mux_tgt("reg_mux_tgt"), double_reg_we_rf("double_reg_we_rf"), reg_we_rf("reg_we_rf"), reg_we_dmem("reg_we_dmem"), reg_re_dmem("reg_re_dmem"), reg_mux_alu1("reg_mux_alu1"), reg_mux_alu2("reg_mux_alu2"), reg_func_alu("reg_func_alu"), reg_sign_ext_PC("reg_sign_ext_PC")//, reg_adder2("reg_adder2")
{   
   //Control Word
   reg_instr_out.clock(this->clock);
   reg_instr_out.in(this->instr_out);
   reg_instr_out.out_opcode(this->sig_opcode);
   reg_instr_out.out_tgt_addr(this->sig_tgt_addr);
   reg_instr_out.out_src1_addr(this->sig_src1_addr);
   reg_instr_out.out_mux2x3_in1(this->sig_mux2x3_in1);
   reg_instr_out.out_mux2x3_in2(this->sig_mux2x3_in2);
   reg_instr_out.out_shifter1_ingresso(this->sig_shifter1_ingresso);
   reg_instr_out.out_sign_ext1_ingresso(this->sig_sign_ext1_ingresso);
   reg_instr_out.out_sign_ext2_ingresso(this->sig_sign_ext2_ingresso);

   // register file
   reg1.clock(this->clock);
   reg1.tgt_addr(this->double_pipeline_sig_tgt_addr);
   reg1.src1_addr(this->pipeline_sig_src1_addr);
   reg1.src2_addr(this->reg_in2);
   reg1.tgt(this->pipeline_reg_tgt);
   reg1.we(this->double_pipeline_we_rf);
   reg1.control_ready(this->control_ready);
   reg1.register_file_ready(this->register_file_ready);
   reg1.src1(this->reg_out1);
   reg1.src2(this->reg_out2);

   //Pipeline register rf tgt
   reg_rf_tgt.clock(this->clock);
   reg_rf_tgt.in1(this->reg_tgt);
   reg_rf_tgt.out1(this->pipeline_reg_tgt);
   
   //Pipeline register shifter1
   reg_shifter1.clock(this->clock);
   reg_shifter1.in1(this->sig_shifter1_ingresso);
   reg_shifter1.out1(this->pipeline_sig_shifter1_ingresso);

   //Pipeline register sign_ext1
   //~ reg_sign_ext1.clock(this->clock);
   //~ reg_sign_ext1.in1(this->sig_sign_ext1_ingresso);
   //~ reg_sign_ext1.out1(this->pipeline_sig_sign_ext1_ingresso);

   //Pipeline register sign_ext2
   reg_sign_ext2.clock(this->clock);
   reg_sign_ext2.in1(this->sig_sign_ext2_ingresso);
   reg_sign_ext2.out1(this->pipeline_sig_sign_ext2_ingresso);

   //Pipeline register register file tgt
   reg_tgt_addr.clock(this->clock);
   reg_tgt_addr.in1(this->sig_tgt_addr);
   reg_tgt_addr.out1(this->pipeline_sig_tgt_addr);

   //Pipeline (two cycles) register register file tgt
   double_reg_tgt_addr.clock(this->clock);
   double_reg_tgt_addr.in1(this->pipeline_sig_tgt_addr);
   double_reg_tgt_addr.out1(this->double_pipeline_sig_tgt_addr);

   //Pipeline register register file src1
   reg_src1_addr.clock(this->clock);
   reg_src1_addr.in1(this->sig_src1_addr);
   reg_src1_addr.out1(this->pipeline_sig_src1_addr);

   //Pipeline register mux2x3_in1
   reg_mux2x3_in1.clock(this->clock);
   reg_mux2x3_in1.in1(this->sig_mux2x3_in1);
   reg_mux2x3_in1.out1(this->pipeline_sig_mux2x3_in1);   

   //Pipeline register mux2x3_in2
   reg_mux2x3_in2.clock(this->clock);
   reg_mux2x3_in2.in1(this->sig_mux2x3_in2);
   reg_mux2x3_in2.out1(this->pipeline_sig_mux2x3_in2); 

   //Pipeline register mux_pc
   //~ reg_mux_pc.clock(this->clock);
   //~ reg_mux_pc.in1(this->mux_pc);
   //~ reg_mux_pc.out1(this->pipeline_mux_pc);

   //Pipeline register mux_alu1
   reg_mux_alu1.clock(this->clock);
   reg_mux_alu1.in1(this->mux_alu1);
   reg_mux_alu1.out1(this->pipeline_mux_alu1);   

   //Pipeline register mux_alu2
   reg_mux_alu2.clock(this->clock);
   reg_mux_alu2.in1(this->mux_alu2);
   reg_mux_alu2.out1(this->pipeline_mux_alu2);

   //Pipeline register func_alu
   reg_func_alu.clock(this->clock);
   reg_func_alu.in1(this->func_alu);
   reg_func_alu.out1(this->pipeline_func_alu);

   //Pipeline register we_dmem
   reg_we_dmem.clock(this->clock);
   reg_we_dmem.in1(this->we_dmem);
   reg_we_dmem.out1(this->pipeline_we_dmem);

   //Pipeline register re_dmem
   reg_re_dmem.clock(this->clock);
   reg_re_dmem.in1(this->re_dmem);
   reg_re_dmem.out1(this->pipeline_re_dmem);

   //Pipeline register we_rf
   reg_we_rf.clock(this->clock);
   reg_we_rf.in1(this->we_rf);
   reg_we_rf.out1(this->pipeline_we_rf);

   //Pipeline register we_rf
   double_reg_we_rf.clock(this->clock);
   double_reg_we_rf.in1(this->pipeline_we_rf);
   double_reg_we_rf.out1(this->double_pipeline_we_rf);
   
   //Pipeline register mux_tgt
   reg_mux_tgt.clock(this->clock);
   reg_mux_tgt.in1(this->mux_tgt);
   reg_mux_tgt.out1(this->pipeline_mux_tgt);

   //Pipeline register mux_rf
   reg_mux_rf.clock(this->clock);
   reg_mux_rf.in1(this->mux_rf);
   reg_mux_rf.out1(this->pipeline_mux_rf);

   //Synchronization register (debugging)
   //reg_mux_rf_dummy.clock(this->clock);
   //reg_mux_rf_dummy.in1(this->mux_rf);
   //reg_mux_rf_dummy.out1(this->dummy_mux_rf);

   // control unit
   cu1.clock(this->clock);
   cu1.eq(this->ALU_outeq);
   cu1.op_code(this->sig_opcode); 
   cu1.load(this->load);
   cu1.mux_pc(this->mux_pc);
   cu1.mux_alu1(this->mux_alu1);
   cu1.mux_alu2(this->mux_alu2);
   cu1.func_alu(this->func_alu);
   cu1.we_dmem(this->we_dmem);
   cu1.re_dmem(this->re_dmem);
   cu1.we_rf(this->we_rf);
   cu1.mux_tgt(this->mux_tgt);
   cu1.mux_rf(this->mux_rf);
   cu1.control_ready(this->control_ready);

   // program counter
   pc1.clock(this->clock);
   pc1.in_addr(this->PC_in);
   pc1.out_addr(this->PC_out);
   pc1.control_ready(this->control_ready);

   // alu
   alu1.src1(this->ALU_in1);
   alu1.src2(this->ALU_in2);
   alu1.control(this->pipeline_func_alu);
   alu1.result(this->ALU_out);
   alu1.equality(this->ALU_outeq);

   // adders
   adder1.a(this->PC_out);
   adder1.b(this->const1);
   adder1.s(this->add1_out);
   adder2.a(this->add1_out);
   adder2.b(this->pipeline_ext_add);
   adder2.s(this->add_out);
   
   //register for pc+1 value
   //~ reg_add1_out.clock(this->clock);
   //~ reg_add1_out.in1(this->add1_out);
   //~ reg_add1_out.out1(this->pipeline_add1_out);
   
   //Pipeline register adder2
//   reg_adder2.clock(this->clock);
//   reg_adder2.in1(this->add1_out);
//   reg_adder2.out1(this->pipeline_add1_out);

   // data memory
   memory_data.clock(this->clock);
   memory_data.data_in(this->reg_out2);
   memory_data.addr(this->ALU_out);
   memory_data.data_out(this->data_out);
   memory_data.write_en(this->pipeline_we_dmem);
   memory_data.read_en(this->pipeline_re_dmem);

   // instruction memory
   memory_instr.clock(this->clock);
   memory_instr.data_in(this->zero_for_data_in_imem);
   memory_instr.addr(this->PC_out);
   memory_instr.data_out(this->instr_out);
   memory_instr.write_en(this->zero_for_we_imem);
   memory_instr.read_en(this->load);

   // multiplexer src2 of Register File
   mux2x3.in1(this->pipeline_sig_mux2x3_in1);
   mux2x3.in2(this->pipeline_sig_mux2x3_in2);
   mux2x3.sel(this->pipeline_mux_rf);
   mux2x3.result(this->reg_in2);

   // multiplexer src1 ALU
   mux2x16_1.in1(this->reg_out1);
   mux2x16_1.in2(this->shift_out);
   mux2x16_1.sel(this->pipeline_mux_alu1);
   mux2x16_1.result(this->ALU_in1);

   // multiplexer src2 ALU
   mux2x16_2.in1(this->reg_out2);
   mux2x16_2.in2(this->ext_alu);
   mux2x16_2.sel(this->pipeline_mux_alu2);
   mux2x16_2.result(this->ALU_in2);

   // multiplexer PC
   mux3x16_1.in1(this->add1_out);
   mux3x16_1.in2(this->add_out);
   mux3x16_1.in3(this->ALU_out);
   mux3x16_1.sel(this->mux_pc);
   mux3x16_1.result(this->PC_in);

   // multiplexer Register File
   mux3x16_2.in1(this->ALU_out);
   mux3x16_2.in2(this->data_out);
   mux3x16_2.in3(this->add1_out);
   mux3x16_2.sel(this->pipeline_mux_tgt);
   mux3x16_2.result(this->reg_tgt);

   // shifter
   shifter1.ingresso(this->pipeline_sig_shifter1_ingresso);
   shifter1.uscita(this->shift_out);

   // sign extension adder PC
   sign_ext1.ingresso(this->sig_sign_ext1_ingresso);
   sign_ext1.uscita(this->ext_add);

   // sign extension mux ALU
   sign_ext2.ingresso(this->pipeline_sig_sign_ext2_ingresso);
   sign_ext2.uscita(this->ext_alu);

   //Pipeline register sign_ext_PC
   reg_sign_ext_PC.clock(this->clock);
   reg_sign_ext_PC.in1(this->ext_add);
   reg_sign_ext_PC.out1(this->pipeline_ext_add);

}

