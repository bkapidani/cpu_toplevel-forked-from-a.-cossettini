#include <systemc.h>
#include "Control_Word.hpp"

using namespace std;

void Control_Word ::route()
{
   sc_bv<16> router;
   router=in.read();

   out_opcode.write(router.range(15,13));
   out_tgt_addr.write(router.range(12,10));
   out_src1_addr.write(router.range(9,7));
   out_mux2x3_in1.write(router.range(2,0));
   out_mux2x3_in2.write(router.range(12,10));
   out_shifter1_ingresso.write("000000000" & router.range(6,0));
   out_sign_ext1_ingresso.write(router.range(6,0));
   out_sign_ext2_ingresso.write(router.range(6,0));

}


