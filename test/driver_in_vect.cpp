//test of CPU toplevel
#include <systemc>
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include "cpu_toplevel.hpp"
#include "toplevel_stimulus.hpp"
#include "Control_Word.hpp"

#define DIM_V1 16
//#define DIM_V2 2

using namespace std;

int sc_main(int argc, char* argv[])
{
   //Necessary test signals
   unsigned v1, v2, m, n_instr;
   unsigned k=0;
   sc_clock clock("clock", 1, 0.5, 0, false);
   sc_signal<sc_bv<16> > one;
   sc_signal<sc_bv<1> > load;
   sc_signal<sc_bv<1> > zero;
   sc_signal<sc_bv<3> > sig_op;
   sc_signal<sc_bv<16> > zero_bv;

   string bin_file, output, instr_line;
   sc_bv<16> dummy_mem;
   
   output = "results_in_vect.dat";
   
   //vector initialization
   sc_uint<16> vect1[DIM_V1]={2,1,2,1,2,5,6,0,4,4,9,21,1,7,15,0};
   //~ sc_uint<16> vect2[DIM_V2]={1,2};

   if (argc < 4)
   {
      cout << "Use is [number of instructions] [stop index of sequence] [starting address in memory]" << endl;
      exit(EXIT_FAILURE);
   }
   else
   {
      bin_file=argv[1];
      n_instr=atoi(argv[2]);
      v1=atoi(argv[3]);
      v2=atoi(argv[4]);
   }   

   //test memory declaration
   Cpu_toplevel Test_CPU("Test_CPU");

   //Debug
   cout << endl << "DEBUG:	RiSC machine has been declared!" << endl;

   //test CPU port mapping
   Test_CPU.clock(clock);
   Test_CPU.load(load);
   Test_CPU.zero_for_we_imem(zero);
   Test_CPU.zero_for_data_in_imem(zero_bv);
   Test_CPU.const1(one);

   for(m=1;m<8;m++)
   {
      Test_CPU.reg1.MemData[m]=0;
   }

   Test_CPU.reg1.MemData[1]=v1;	 	//Pointer to first vector in R1
   Test_CPU.reg1.MemData[2]=v2;		//Pointer to second vector in R2
   
      
   cout << endl;
   cout << "Arg1: " << Test_CPU.reg1.MemData[1] << endl;
   cout << "Arg2: " << Test_CPU.reg1.MemData[2] << endl;

   //Debug
   cout << endl << "DEBUG:	RiSC has been mapped!" << endl;

   ifstream is(bin_file.c_str());

   while(k<n_instr && getline(is,instr_line))
   {
      dummy_mem = instr_line.c_str();
      cout << "Dummy_mem: " << dummy_mem << endl;
      Test_CPU.memory_instr.MemCell[k].value = dummy_mem;
      k++;
   }
   cout << endl;
   k=0;
   
   while(k<DIM_V1)
   {
      dummy_mem = vect1[k];		//overloaded assignment
      cout << "vector element: " << dummy_mem << endl;
      cout << v1+k << endl;
      Test_CPU.memory_data.MemCell[k].value = dummy_mem;
      k++;
   }
   cout << endl;
   k=0;
//~ 
   //~ while(k<DIM_V2)
   //~ {
      //~ dummy_mem = vect2[k];		//overloaded assignment
      //~ cout << "vector element: " << dummy_mem << endl;
      //~ Test_CPU.memory_data.MemCell[v2+k].value = dummy_mem;
      //~ k++;
   //~ }
   //~ cout << endl;
   //~ k=0;

   //Stimulus declaration
   toplevel_stimulus Test_Stim("CPU_STIM");

   //Debug
   cout << endl << "DEBUG:	Stimulus has been declared!" << endl;

   //Stimulus port mapping
   Test_Stim.clock(clock);
   Test_Stim.num_instr=k;
   Test_Stim.operation(Test_CPU.sig_opcode);
   Test_Stim.load(load);
   Test_Stim.zero(zero);
   Test_Stim.zero_bv(zero_bv);
   Test_Stim.const1(one);

   //Debug
   cout << endl << "DEBUG:	Stimulus has been mapped!" << endl;

   //preparing simulation output
   sc_trace_file *wf = sc_create_vcd_trace_file("in_vect_waveforms");

   //tracing test waveforms
   sc_trace(wf, clock, "clock");
   sc_trace(wf, Test_CPU.instr_out, "instr_out");
   sc_trace(wf, Test_CPU.sig_opcode, "opcode");
   sc_trace(wf, Test_CPU.PC_in, "PC_in");
   sc_trace(wf, Test_CPU.add_out, "add_out");
   sc_trace(wf, Test_CPU.PC_out, "PC_out");
   sc_trace(wf, Test_CPU.mux_pc, "mux_PC");
   sc_trace(wf, Test_CPU.pipeline_func_alu, "pipeline_func_alu");
   sc_trace(wf, Test_CPU.ALU_in1, "ALU_in1");
   sc_trace(wf, Test_CPU.ALU_in2, "ALU_in2");
   sc_trace(wf, Test_CPU.ALU_out, "ALU_out");
   sc_trace(wf, Test_CPU.ALU_outeq, "eq");
   sc_trace(wf, Test_CPU.cu1.beq_check, "beq_check");
   sc_trace(wf, Test_CPU.reg_in2, "reg_in2");
   sc_trace(wf, Test_CPU.pipeline_sig_src1_addr, "pipeline_sig_src1_addr");
   sc_trace(wf, Test_CPU.double_pipeline_sig_tgt_addr, "tgt_addr");
   sc_trace(wf, Test_CPU.data_out, "Data_mem_out");
   sc_trace(wf, Test_CPU.memory_data.mux_data_in.at(1), "value_1");
   sc_trace(wf, Test_CPU.memory_data.mux_data_in.at(8), "value_2");
   sc_trace(wf, Test_CPU.pipeline_reg_tgt, "rf_write_value");
   sc_trace(wf, Test_CPU.double_pipeline_we_rf, "pipeline_we_rf");
   sc_trace(wf, Test_CPU.reg_out1, "reg_out1");
   sc_trace(wf, Test_CPU.reg_out2, "reg_out2");
   sc_trace(wf, Test_CPU.pipeline_we_dmem, "pipeline_we_dmem");
   sc_trace(wf, Test_CPU.pipeline_re_dmem, "pipeline_re_dmem");
   sc_trace(wf, Test_CPU.ext_add, "immediate_operand");
   
   //Debug
   cout << endl << "DEBUG:	About to start simulation!" << endl;

   //simulate!
   sc_start();

   //Debug
   cout << endl << "DEBUG:	Simulation has actually progressed!" << endl;

   //Debug
   cout << endl << "DEBUG:	Simulation ended!" << endl;
   
   cout << endl;
   unsigned jj=0;

   ofstream os(output.c_str());

   for (jj=0;jj<8;jj++)
   {
      os << "R" << jj << ":   " << Test_CPU.reg1.MemData[jj].to_uint() << endl;
   }
	
   os << endl;

   unsigned dummy_uint;
   jj=v1;
   dummy_uint=Test_CPU.memory_data.MemCell[jj].value.to_uint();
   
   os << "1st Vector:   ";
   while(dummy_uint != 0)
   {
      os << dummy_uint << "  ";
      jj++;
      dummy_uint=Test_CPU.memory_data.MemCell[jj].value.to_uint();
      
   }
   os << endl << endl;
   
   jj=v2;
   dummy_uint=Test_CPU.memory_data.MemCell[jj].value.to_uint();
   
   os << "2nd Vector:   ";
   while(dummy_uint != 0)
   {
      os << dummy_uint << "  ";
      jj++;
  	  dummy_uint=Test_CPU.memory_data.MemCell[jj].value.to_uint();  
   }
   os << endl << endl;

   sc_close_vcd_trace_file(wf);
   is.close();
   os.close();

   return 0;
}
