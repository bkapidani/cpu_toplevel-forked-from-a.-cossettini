//test of CPU toplevel
#include <systemc>
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include "cpu_toplevel.hpp"
#include "toplevel_stimulus.hpp"
#include "Control_Word.hpp"

using namespace std;

int sc_main(int argc, char* argv[])
{
   //Necessary test signals
   unsigned n, ind, m, n_instr;
   unsigned k=0;
   sc_clock clock("clock", 1, 0.5, 0, false);
   sc_signal<sc_bv<16> > one;
   sc_signal<sc_bv<1> > load;
   sc_signal<sc_bv<1> > zero;
   sc_signal<sc_bv<3> > sig_op;
   sc_signal<sc_bv<16> > zero_bv;


   string bin_file, instr_line, output;
   sc_bv<16> dummy_mem;

   output = "results_fibo.dat";

   if (argc < 4)
   {
      cout << "Use is [number of instructions] [stop index of sequence] [starting address in memory]" << endl;
      exit(EXIT_FAILURE);
   }
   else
   {
      bin_file=argv[1];
      n_instr=atoi(argv[2]);
      n=atoi(argv[3]);
      ind=atoi(argv[4]);   
   }   

   //test memory declaration
   Cpu_toplevel Test_CPU("Test_CPU");

   //Debug
   cout << endl << "DEBUG:	RiSC machine has been declared!" << endl;

   //test CPU port mapping
   Test_CPU.clock(clock);
   Test_CPU.load(load);
   Test_CPU.zero_for_we_imem(zero);
   Test_CPU.zero_for_data_in_imem(zero_bv);
   Test_CPU.const1(one);

   for(m=1;m<8;m++)
   {
      Test_CPU.reg1.MemData[m]=0;
   }

   Test_CPU.reg1.MemData[1]=n;	 		//writes index n into R1
   Test_CPU.reg1.MemData[2]=ind;		//writes memory address into R2

   //Debug
   cout << endl << "DEBUG:	RiSC has been mapped!" << endl;

   ifstream is(bin_file.c_str());

   while(k<n_instr && getline(is,instr_line))
   {
      dummy_mem = instr_line.c_str();
      cout << "Dummy_mem: " << dummy_mem << endl;
      Test_CPU.memory_instr.MemCell[k].value = dummy_mem;
      k++;
   }
   
   cout << endl;
   cout << "Arg1: " << Test_CPU.reg1.MemData[1] << endl;
   cout << "Arg2: " << Test_CPU.reg1.MemData[2] << endl;

   //Stimulus declaration
   toplevel_stimulus Test_Stim("CPU_STIM");

   //Debug
   cout << endl << "DEBUG:	Stimulus has been declared!" << endl;

   //Stimulus port mapping
   Test_Stim.clock(clock);
   Test_Stim.num_instr=k;
   Test_Stim.operation(Test_CPU.sig_opcode);
   Test_Stim.load(load);
   Test_Stim.zero(zero);
   Test_Stim.zero_bv(zero_bv);
   Test_Stim.const1(one);

   //Debug
   cout << endl << "DEBUG:	Stimulus has been mapped!" << endl;

   //preparing simulation output
   sc_trace_file *wf = sc_create_vcd_trace_file("fibo_waveforms");

   //tracing test waveforms
   sc_trace(wf, clock, "clock");
   sc_trace(wf, Test_CPU.instr_out, "instr_out");
   sc_trace(wf, Test_CPU.sig_opcode, "opcode");
   sc_trace(wf, Test_CPU.PC_in, "PC_in");
   sc_trace(wf, Test_CPU.load, "load");
   sc_trace(wf, Test_CPU.add_out, "add_out");
   sc_trace(wf, Test_CPU.PC_out, "PC_out");
   sc_trace(wf, Test_CPU.mux_pc, "mux_PC");
   sc_trace(wf, Test_CPU.pipeline_func_alu, "pipeline_func_alu");
   sc_trace(wf, Test_CPU.ALU_in1, "ALU_in1");
   sc_trace(wf, Test_CPU.ALU_in2, "ALU_in2");
   sc_trace(wf, Test_CPU.ALU_out, "ALU_out");
   sc_trace(wf, Test_CPU.ALU_outeq, "eq");
   sc_trace(wf, Test_CPU.cu1.beq_check, "beq_check");
   sc_trace(wf, Test_CPU.reg_in2, "reg_in2");
   sc_trace(wf, Test_CPU.pipeline_sig_src1_addr, "pipeline_sig_src1_addr");
   sc_trace(wf, Test_CPU.double_pipeline_sig_tgt_addr, "tgt_addr");
   sc_trace(wf, Test_CPU.pipeline_reg_tgt, "rf_write_value");
   sc_trace(wf, Test_CPU.double_pipeline_we_rf, "pipeline_we_rf");
   sc_trace(wf, Test_CPU.reg_out1, "reg_out1");
   sc_trace(wf, Test_CPU.reg_out2, "reg_out2");
   sc_trace(wf, Test_CPU.pipeline_we_dmem, "pipeline_we_dmem");
   sc_trace(wf, Test_CPU.pipeline_ext_add, "immediate_operand");
   sc_trace(wf, Test_CPU.add_out, "add_out");
   sc_trace(wf, Test_CPU.add1_out, "add1_out");
   sc_trace(wf, Test_CPU.cu1.jalr_check, "JALR_check");
   
   //Debug
   cout << endl << "DEBUG:	About to start simulation!" << endl;

   //simulate!
   sc_start();

   //Debug
   cout << endl << "DEBUG:	Simulation has actually progressed!" << endl;

   //Debug
   cout << endl << "DEBUG:	Simulation ended!" << endl;
   
   cout << endl;
   unsigned jj=0;

   ofstream os(output.c_str());

   for (jj=0;jj<8;jj++)
   {
      os << "R" << jj << ":   " << Test_CPU.reg1.MemData[jj].to_uint() << endl;
   }
   
   os << endl;
   
   os << "M" << ind << ":   " << Test_CPU.memory_data.MemCell[ind].value.to_uint() << endl;   

   os << endl;
   
   sc_close_vcd_trace_file(wf);
   is.close();
   os.close();

   return 0;
}
