cmake_minimum_required (VERSION 2.6)

add_subdirectory ("alu")

#add_subdirectory ("adder")

add_subdirectory ("cpu")

add_subdirectory ("memory")

#add_subdirectory ("multiplexers")

add_subdirectory ("sign-extend-7")

add_subdirectory ("left-shift-6")

add_executable (driver_fibo test/driver_fibo.cpp)

add_executable (driver_in_vect test/driver_in_vect.cpp)

include_directories ("include" "alu/include")

include_directories ("include" "adder/include")

include_directories ("include" "cpu/include")

include_directories ("include" "memory/include")

include_directories ("include" "multiplexers/include")

include_directories ("include" "sign-extend-7/include")

include_directories ("include" "left-shift-6/include")

add_library (cpu_toplevel "src/cpu_toplevel.cpp")

add_library (Control_Word "src/Control_Word.cpp")

add_library (toplevel_stimulus "src/toplevel_stimulus.cpp")

target_link_libraries (driver_fibo toplevel_stimulus cpu_toplevel Control_Word alu multiplexer_3x16 adder multiplexer_2 multiplexer_3 multiplexer_3_2 register_file control_unit program_counter T_Register adder_cpu shifter AND_1 MUX CELL DEC RAM STM sign multiplexer_2x3 multiplexer_2x16 multiplexer_3x16 systemc)

target_link_libraries (driver_in_vect toplevel_stimulus cpu_toplevel Control_Word alu multiplexer_3x16 adder multiplexer_2 multiplexer_3 multiplexer_3_2 register_file control_unit program_counter T_Register adder_cpu shifter AND_1 MUX CELL DEC RAM STM sign multiplexer_2x3 multiplexer_2x16 multiplexer_3x16 systemc)

enable_testing ()

add_test (test_toplevel_1 driver_fibo ../test/fibo.dat 27 5 2)

add_test (test_toplevel_2 driver_in_vect ../test/in_vector.dat 26 1 8)

